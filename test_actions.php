<?php
?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
	crossorigin="anonymous">

<script src="../_js/jquery-3.1.0.min.js"></script>

<link rel="stylesheet"
	href="https://code.jquery.com/ui/1.12.0/themes/ui-lightness/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
</head>
<body>

	<div class="container">

		<form action="test_actions_handler.php" method="post"
			enctype='multipart/form-data'>

			<h1>ЦДО. Тестирование функций</h1>

			<div class="row">

				<div class="col-xs-3">
					<button type="submit" class="btn btn-success" name="load_manual">Загрузить
						квитанции вручную</button>
				</div>

				<div class="col-xs-6">
					<input type="file" multiple="true" class="btn btn-success"
						name="filer[]" style="width: 100%">
				</div>

			</div>

			<h2></h2>

			<div class="row">
				<div class="col-xs-12">
					<button type="submit" class="btn btn-success" name="load_ftp">Загрузить
						квитанции c ftp</button>
				</div>
			</div>

			<h2></h2>

			<!-- onclick='document.location.href="/cdo/test_actions_handler.php?action=drop"; return false;' -->
			<div class="row"> 
				<div class="col-xs-12">
					<button type="submit" class="btn btn-success" name="drop">Очистить
						таблицу квитанций</button>
				</div>
			</div>

			<h2></h2>

			<div class="row">
				<div class="col-xs-12">
					<button type="submit" class="btn btn-success"
						name="ftp_put_payments">Отправить платежи на ftp</button>
				</div>
			</div>

		</form>

	</div>

</body>